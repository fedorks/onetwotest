package onetwotest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * 
 * @author FedorkZ
 * 
 * finds a match in input sequence to a search string (passed to constructor) 
 */

public class WordScanner extends Main {
	private List<MatrixValue<Character>> mStartList = new ArrayList<>(); //All letters are initially here (coords are null here)
	private List<MatrixValue<Character>> mResultList = new ArrayList<>(); //Stores letters that already have a mapped element in matrix (coords are filled)
	private HashMap<Character, Integer> mLetterCnts = new HashMap<>(); //For search optimization, stores quantities of unmapped elements in string  
	/* INVARIANT: Each letter from search-string is in mStartList (if mapped elemet still not found) or in mResultList (if has a mapped element).
	 *            At the end of algorithm if mStartList is empty means that all letters are mapped -> solution found, otherwise - impossible to solve
	 * IMPORTANT: Summ of letters counts in mLetterCnts is equal to mStartList items count.
	 *            Each element in mStartList has an entry in mLetterCnts, and otherwise
	 */            
	
	public WordScanner(String searchText) {
		prepareSearchArray(searchText);
	}
	
	/***
	 * Processes single input element 
	 * @param r - current row
	 * @param c - current col
	 * @param chr - char
	 * @return true if already has an answer (no need to iterate anymore)
	 */
	public boolean processInput(int r, int c, Character rawChr) {
		Character chr = Character.toUpperCase( rawChr );//looks dangerous huh...
//					System.out.println("_" + chr + "_");

		if (mLetterCnts.containsKey(chr)){// this is for optimization purposes
			MatrixValue<Character> letter = findLetter(chr, mStartList);// due to mLetterCnts optimization - newer returns NULL 
			
			if (letter != null){
				letter.coords = new MatrixCoords(r, c);//fill coords
				letter.val = rawChr;// to keep original up/lowCase from Matrix
				
				//Move letter that now have a mach [INVARIANT TRUE]
				mResultList.add(letter);
				mStartList.remove(letter);
				
				//update letter counters
				int cnt = mLetterCnts.get(chr);
				cnt--;
				
				if (cnt<=0){
					mLetterCnts.remove(chr);
				} else {
					mLetterCnts.put(chr, cnt);
				}
			}
		}
		
		return hasAnswer();
	}
	
	public boolean hasAnswer(){
		return mStartList.isEmpty();
	}

	// find by VALUE, null if nothing found.
	private MatrixValue<Character> findLetter(Character chr, List<MatrixValue<Character>> lst) {
		if (chr == null)//check
			return null;
		
		for (MatrixValue<Character> letter : lst){
			if (chr.equals(letter.val)){
				return letter;
			}
		}
		
		return null;//not found
	}

	// init search data
	private void prepareSearchArray(final String str) throws IllegalArgumentException{
		if (str == null)
			throw new IllegalArgumentException("Can't take NULL search string!");
		
		int n = str.length();
		for (int i=0; i<n; i++){
			Character c = Character.toUpperCase(str.charAt(i));
			
			mStartList.add(new MatrixValue<Character>(null, i, c));
			
			if (!mLetterCnts.containsKey(c)){
				mLetterCnts.put(c, 0);
			} 
			
			//increment letter counter
			int v = mLetterCnts.get(c);
			mLetterCnts.put(c, ++v);
		}
		
		// All letters are in mSrartList [INVARIANT TRUE]
	}

////////////////////////////////////////////
	static class MatrixCoords{
		int row;
		int col;

		public MatrixCoords(int r, int c) {
			row = r;
			col = c;
		}
	}
	
	static class MatrixValue<T>{//only srtuct to hold values -> no logic, public fields
		T val;
		MatrixCoords coords;
		int index;
	
		public MatrixValue(int r, int c, T v) {
			val = v;
			coords = new MatrixCoords(r, c);
		}

		public MatrixValue(MatrixCoords mc, int idx, T v) {
			val = v;
			coords = mc;
			index = idx;
		}
		
		public T getValue(){
			return val;
		}
		
		public MatrixCoords getCoords(){
			return coords;
		}
		
		public int getIndex(){
			return index;
		}
	}

	@SuppressWarnings("rawtypes")//we use only index
	public List<MatrixValue<Character>> getResultArray() {
		//Sort by index
		Collections.sort(mResultList, new Comparator<MatrixValue>() {
			@Override
			public int compare(MatrixValue o1, MatrixValue o2) {
				return Integer.compare(o1.index, o2.index);
			}
		});
		
		return mResultList;
	}	
}
