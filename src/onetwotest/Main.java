package onetwotest;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

import onetwotest.WordScanner.MatrixValue;

public class Main {
	private static final String SEARCH_STRING = "onetwotrip";

	private static final String FILENAME = "input.txt";

	private static final String MATRIX_DELIMITER = "";

	private static final String FAIL_OUT_STRING = "Impossible";
	private static final String LETTER_RESULT_FORMAT = "%1$s - (%2$d, %3$d);";
	private static WordScanner mWordScanner;
	
	public static void main(String[] args) {
		FileInputStream fileInputStream = null;
		
		try {
			System.out.println("Working Directory = " +
		              System.getProperty("user.dir"));
			
			mWordScanner = new WordScanner(SEARCH_STRING);
			
			fileInputStream = new FileInputStream(FILENAME);
			processData(fileInputStream);
//			processData(System.in);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fileInputStream != null)
				try {
					fileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		
	}
	
private static void processData(InputStream src) {
	Scanner input = new Scanner(src);
	
	try {
		int rows = input.nextInt();
		int cols = input.nextInt();
		
		System.out.println("Processing " + rows + " x " + cols);
		
		input.useDelimiter(MATRIX_DELIMITER);
		
		for (int r = 0; r < rows; r++) {
			input.nextLine();//TODO: better skip \n\r other way
			for (int c = 0; c < cols; c++) {
				Character chr = input.next().charAt(0);
				if (mWordScanner.processInput(r, c, chr )){
					outputSuccessResultAndExit();
				}
			}
    	}
	} finally {
		if (input != null)
			input.close();
	}
	
	outputFailResult();
}



	private static void outputSuccessResultAndExit() {
		List<MatrixValue<Character>> result = mWordScanner.getResultArray();
		for (MatrixValue<Character> letter : result){
			String line = String.format(LETTER_RESULT_FORMAT, 
					letter.getValue(), 
					letter.getCoords().row, 
					letter.getCoords().col );

			System.out.println(line);
		}
		
		System.exit(0);
	}

	private static void outputFailResult() {
		System.out.println(FAIL_OUT_STRING);
	}
}
